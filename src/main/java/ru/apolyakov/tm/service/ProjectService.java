package ru.apolyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.api.service.IAuthService;
import ru.apolyakov.tm.api.service.IProjectService;
import ru.apolyakov.tm.model.Project;

public final class ProjectService extends AbstractBusinessEntityService<Project> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository, @NotNull final IAuthService authService) {
        super(repository, authService);
    }

}
