package ru.apolyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.service.IAuthService;
import ru.apolyakov.tm.api.service.IProjectService;
import ru.apolyakov.tm.api.service.IProjectTaskService;
import ru.apolyakov.tm.api.service.ITaskService;
import ru.apolyakov.tm.exception.empty.IdEmptyException;
import ru.apolyakov.tm.exception.empty.NameEmptyException;
import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.exception.incorrect.IndexIncorrectException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.ValidationUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(@NotNull final ITaskService taskService, @NotNull final IProjectService projectService, @NotNull final IAuthService authService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Nullable
    @Override
    public final Task addTaskToProject(@NotNull final String projectId, @NotNull final String taskId) {
        if (ValidationUtil.isEmptyStr(projectId) || ValidationUtil.isEmptyStr(taskId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return (taskService.addTaskToProject(taskId, projectId));
    }

    @Override
    public final void clearProjects() {
        projectService.findAll().forEach(project -> taskService.removeAllByProjectId(project.getId()));
        projectService.clear();
    }

    @NotNull
    @Override
    public final List<Task> findAllTasksByProjectId(@NotNull final String projectId, @NotNull final Comparator<Task> taskComparator) {
        if (ValidationUtil.isEmptyStr(projectId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return taskService.findAllByProjectId(projectId, taskComparator);
    }

    @Nullable
    @Override
    public final Project removeProjectById(@Nullable final String projectId) {
        if (ValidationUtil.isEmptyStr(projectId)) throw new IdEmptyException();
        Optional.ofNullable(projectService.findOneById(projectId)).orElseThrow(ProjectNotFoundException::new);
        taskService.removeAllByProjectId(projectId);
        return projectService.removeOneById(projectId);
    }

    @Nullable
    @Override
    public final Project removeProjectByIndex(final int index) {
        if (ValidationUtil.isInvalidListIndex(index, projectService.getSize()))
            throw new IndexIncorrectException(index + 1);
        final String id = projectService.getId(index);
        return removeProjectById(id);
    }

    @Nullable
    @Override
    public final Project removeProjectByName(@NotNull final String name) {
        if (ValidationUtil.isEmptyStr(name)) throw new NameEmptyException();
        return removeProjectById(Optional.ofNullable(projectService.getId(name)).orElseThrow(ProjectNotFoundException::new));
    }

    @Nullable
    @Override
    public final Task removeTaskFromProject(@NotNull final String projectId, @NotNull final String taskId) {
        if (ValidationUtil.isEmptyStr(projectId) || ValidationUtil.isEmptyStr(taskId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return (taskService.removeTaskFromProject(taskId, projectId));
    }

}
