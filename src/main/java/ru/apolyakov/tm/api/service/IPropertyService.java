package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.api.property.IProperty;

import java.io.IOException;

public interface IPropertyService extends IProperty {

    void getPropertiesFromFile() throws IOException;

}
