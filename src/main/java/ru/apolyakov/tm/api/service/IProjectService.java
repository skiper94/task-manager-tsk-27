package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.model.Project;

public interface IProjectService extends IBusinessEntityService<Project> {

}
