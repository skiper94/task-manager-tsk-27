package ru.apolyakov.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull Status getStatus();

    void setStatus(@NotNull Status status);

}
