package ru.apolyakov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class IdEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Id is empty.";

    public IdEmptyException() {
        super(MESSAGE);
    }

}
