package ru.apolyakov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Project not found.";

    public ProjectNotFoundException() {
        super(MESSAGE);
    }

}
