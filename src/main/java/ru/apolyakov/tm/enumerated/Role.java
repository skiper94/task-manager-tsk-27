package ru.apolyakov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMIN("Admin"),
    USER("User");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
