package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-create";

    @NotNull
    private final static String DESCRIPTION = "Creating a new task";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @NotNull final String description = readLine(DESCRIPTION_INPUT);
        getTaskService().add(name, description);
        printLinesWithEmptyLine("Done. Type <tasks-list> to view all tasks.");
    }

}
