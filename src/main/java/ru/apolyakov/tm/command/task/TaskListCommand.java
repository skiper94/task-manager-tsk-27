package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;

import static ru.apolyakov.tm.util.TerminalUtil.*;

public class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "tasks-list";

    @NotNull
    private final static String DESCRIPTION = "Showing all tasks";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void execute() {
        if (getTaskService().isEmpty()) {
            printLinesWithEmptyLine("No tasks found. Type <task-create> to add a task.");
            return;
        }
        @NotNull final Comparator<Task> comparator = readComparator();
        printListWithIndexes(getTaskService().findAll(comparator));
    }

}
