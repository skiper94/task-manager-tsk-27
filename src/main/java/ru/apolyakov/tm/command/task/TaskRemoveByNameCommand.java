package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-remove-by-name";

    @NotNull
    private final static String DESCRIPTION = "Removing task by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getTaskService().removeOneByName(name));
    }

}
