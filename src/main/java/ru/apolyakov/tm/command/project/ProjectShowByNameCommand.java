package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.command.AbstractCommand;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-show-by-name";

    @NotNull
    private final static String DESCRIPTION = "Showing project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = TerminalUtil.readLine(AbstractCommand.NAME_INPUT);
        @Nullable final Project project = getProjectService().findOneByName(name);
        showProject(project);
    }

}
