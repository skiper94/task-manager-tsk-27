package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.service.IProjectService;
import ru.apolyakov.tm.api.service.IProjectTaskService;
import ru.apolyakov.tm.command.AbstractCommand;
import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.exception.other.ServiceLocatorNotFoundException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    {
        setNeedAuthorization(true);
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    protected IProjectService getProjectService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getProjectTaskService();
    }

    protected void showProject(@Nullable final Project project) {
        TerminalUtil.printLinesWithEmptyLine(Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new));
    }

    protected void throwExceptionIfNull(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
