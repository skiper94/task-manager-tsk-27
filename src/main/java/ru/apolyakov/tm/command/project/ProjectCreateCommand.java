package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-create";

    @NotNull
    private final static String DESCRIPTION = "Creating a new project";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @NotNull final String description = readLine(DESCRIPTION_INPUT);
        getProjectService().add(name, description);
        TerminalUtil.printLinesWithEmptyLine("Done. Type <list projects> to view all projects.");
    }

}
