package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.security.UserSelfDeleteNotAllowedException;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class UserRemoveByIdCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "Removing user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        if (id.equals(getAuthService().getUserId())) throw new UserSelfDeleteNotAllowedException();
        throwExceptionIfNull(getUserService().removeOneById(id));
    }

}
