package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Role;

import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class UserUnlockByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-unlock-by-login";

    @NotNull
    private static final String DESCRIPTION = "unlocking user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        if (!getUserService().unlockByLogin(login)) printLinesWithEmptyLine(USER_IS_NOT_LOCKED);
    }

}
