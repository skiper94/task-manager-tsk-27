package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.model.User;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class UserShowByIdCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Showing user by ID";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        @Nullable final User user = getUserService().findOneById(id);
        showUser(user);
    }

}
