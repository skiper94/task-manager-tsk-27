package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Role;

import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.TerminalUtil.printList;

public final class UserListCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "users-list";

    @NotNull
    private static final String DESCRIPTION = "Showing all users";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        if (getUserService().isEmpty()) {
            printLinesWithEmptyLine("No users found!");
            return;
        }
        printList(getUserService().findAll());
    }

}
