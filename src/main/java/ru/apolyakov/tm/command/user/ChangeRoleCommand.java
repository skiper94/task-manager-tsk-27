package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Role;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;
import static ru.apolyakov.tm.util.TerminalUtil.readRole;

public final class ChangeRoleCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "role-change";

    @NotNull
    private static final String DESCRIPTION = "Changing user's role";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final Role role = readRole(ENTER_ROLE);
        getUserService().setRole(login, role);
    }

}
