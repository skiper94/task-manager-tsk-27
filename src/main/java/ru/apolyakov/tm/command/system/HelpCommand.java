package ru.apolyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.command.AbstractCommand;

import static ru.apolyakov.tm.constant.StringConst.APP_HELP_HINT_TEXT;
import static ru.apolyakov.tm.util.TerminalUtil.printList;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "help";

    @NotNull
    private final static String ARG_NAME = "-h";

    @NotNull
    private final static String DESCRIPTION = "Show all commands and args";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        System.out.println(APP_HELP_HINT_TEXT);
        printList(getCommandService().getCommandList());
    }

}
