package ru.apolyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractModelRepository<E extends AbstractModel> implements IRepository<E> {

    @NotNull
    protected final Map<String, E> entities = new LinkedHashMap<>();

    @Override
    public void add(@NotNull final E entity) {
        entities.put(entity.getId(), entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    public void remove(final @NotNull E entity) {
        entities.remove(entity.getId());
    }

    @Nullable
    @Override
    public E removeOneById(@NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findOneById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        if (entities == null) return;
        @NotNull final Map<String, E> map = entities.stream().
                collect(Collectors.toMap(E::getId, e -> e));
        this.entities.putAll(map);
    }

    @Override
    public void clear() {
        entities.clear();
    }


    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public boolean isEmpty() {
        return entities.isEmpty();
    }

}
